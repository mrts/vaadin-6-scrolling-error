# Vaadin 6 form scrolling error example

**THIS ERROR HAS BEEN FIXED IN VAADIN 6.8.18**

When using Vaadin 6 applications in the Chrome browser, scrolling causes
internal errors. The errors also happen when page zoom is not 100%.

This is caused by the browser sending float values for integer variables in
UIDL requests.

See also https://vaadin.com/forum#!/thread/8755632. There's an invasive
workaround presented there, but this is not a good approach for production
applications.

## This project

This example project demonstrates the error. It was created with the standard
Vaadin 6 project generator in Eclipse.

## Build and run

1. Open project in Eclipse in Java EE perspective
2. Create a new server in *Servers* tab (tested with WildFly 10.1, you need
   JBoss Tools)
3. Select project and click the *Run* button in Eclipse or choose
   *Run As > Run on Server*
4. When you change anything, right-click the *ScrollingError* web module under
   WildFly in *Servers* tab and choose *Full Publish*, then *Run*

## Steps to reproduce the error

1. Run the application
2. Open http://localhost:8080/ScrollingError/ in Google Chrome (the error seems
   to be Chrome-specific)
3. Click *Show form*
4. A form opens, make sure that it has a scrollbar; scroll down a bit and
   select any dropdown
   - if the error does not happen immediately, scroll down more and try again

Result:

![](doc/error.png)

Exception:

    Terminal error: java.lang.RuntimeException: Could not convert variable "scrollTop" for com.vaadin.ui.Window (PID4)
	at com.vaadin.terminal.gwt.server.AbstractCommunicationManager.decodeVariable(AbstractCommunicationManager.java:1583)

    Caused by: java.lang.NumberFormatException: For input string: "109.6"


The parsing code looks as follows in `AbstractCommunicationManager.decodeVariable`:

        case VTYPE_INTEGER:
            val = Integer.valueOf(strValue);
            break;

An easy fix would be to use `Double.valueOf(strValue).intValue()` instead, but
a better fix would be at client-side to avoid sending fractions in the first
place.
