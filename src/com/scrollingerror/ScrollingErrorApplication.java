package com.scrollingerror;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.vaadin.Application;
import com.vaadin.ui.*;

/**
 * Main application class.
 */
@SuppressWarnings("serial")
public class ScrollingErrorApplication extends Application {

	@Override
	public void init() {
		final Window mainWindow = new Window("Scrolling Error Application");
		final Label label = new Label("This application demonstrates scrolling errors in Vaadin 6 and Chrome. " +
				"Click on the <i>Show form</i> button, scroll down a bit and select any drop-down to see the error.",
				Label.CONTENT_XHTML);
		mainWindow.addComponent(label);
		final Button button = new Button("Show form", e -> {
			showForm(mainWindow);
		});
		mainWindow.addComponent(button);
		setMainWindow(mainWindow);
	}

	private void showForm(final Window mainWindow) {
		final Form form = new Form();

		IntStream.rangeClosed('!', '}')
			.mapToObj(c -> String.valueOf((char) c))
			.collect(Collectors.toList()).forEach(s -> {
				final Select select = new Select("Select " + s);
				select.addItem("1");
				select.addItem("2");
				select.addItem("3");
				form.addField(s, select);
			});

		final Window formWindow = new Window();
		formWindow.setModal(true);
		final Field field = form.getField("A");
		formWindow.setWidth(300, field.getWidthUnits());

		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(form);
		formWindow.addComponent(layout);

		mainWindow.addWindow(formWindow);
	}

}
